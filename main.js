const electron = require("electron")
const $ = require('jquery')

const app= electron.app
const BrowserWindow = electron.BrowserWindow
const path = require("path")
const url = require("url")

let win
function createWindow() {
    win = new BrowserWindow({        
        width: 1500,
        height: 800})
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file',
        slashes: true,
        nodeIntegration: true,
        
        
    }))

    win.setMenu(null);
    
    win.webContents.openDevTools()
    win.on('closed', ()=>{
        win = null
    })
}







app.on('ready', createWindow)